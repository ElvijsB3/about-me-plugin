<?php
/*
Plugin Name: Autora kaste
Plugin URI: http://www.lost.lv
Description: Remake of simple, but great ABAP plugin.
Version: 1.0
Author: Jeriff Cheng
Author URI: http://profiles.wordpress.org/jeriff-cheng/
*/



add_action( 'wp_enqueue_scripts', 'add_authorbox_css' );	
function add_authorbox_css() {
	wp_enqueue_style( 'abap-css', plugins_url( 'akaste.css', __FILE__ ) );
}
	
add_filter('user_contactmethods','abap_user_contactmethods',10,1);	
function abap_user_contactmethods( $contactmethods ) {

  if ( isset( $contactmethods['twitter'] ) )
    unset( $contactmethods['twitter'] );
	
  if ( isset( $contactmethods['googleplus'] ) )
    unset( $contactmethods['googleplus'] );	
	
  if ( isset( $contactmethods['linkedin'] ) )
    unset( $contactmethods['linkedin'] );	

  if ( isset( $contactmethods['youtube'] ) )
    unset( $contactmethods['youtube'] );	
	
  if ( isset( $contactmethods['instagram'] ) )
    unset( $contactmethods['instagram'] );		
	

	/* Add user contact methods */
	
  if ( !isset( $contactmethods['twitter'] ) )
    $contactmethods['twitter'] = __('Twitter Username'); 
	
  if ( !isset( $contactmethods['googleplus'] ) )
    $contactmethods['googleplus'] = __('Google + URL'); 
	
  if ( !isset( $contactmethods['linkedin'] ) )
    $contactmethods['linkedin'] = __('Linkedin URL'); 

  if ( !isset( $contactmethods['instagram'] ) )
    $contactmethods['instagram'] = __('Instagram URL'); 	

  if ( !isset( $contactmethods['abap_avatar'] ) )
    $contactmethods['abap_avatar'] = __('Custom Avatar Image URL'); 	
	
	return $contactmethods;
}

//Custom Avatar
add_filter( 'get_avatar', 'abap_get_avatar', 10, 3 );
function abap_get_avatar( $avatar, $id_or_email, $size ) {
	if (get_the_author_meta('abap_avatar')){
		$avatar = '<img src="'. get_the_author_meta('abap_avatar').'" alt="' . get_the_author() . '" width="' . $size . '" height="' . $size . '" />';
	}
    return $avatar;
}


add_filter('the_content', 'add_author_box');
function add_author_box($content) {
	//Define the Main Part of Author Box
	$author_box='<div id="abap_box">
	
	<p><span class="author_photo">'.get_avatar(get_the_author_meta('ID') ).'</span><a rel="nofollow" href="'.get_author_posts_url(get_the_author_meta( 'ID' )).'"><h3 class="eblog_author-name">'.get_the_author_meta('display_name').'</h3></a> <br>'.get_the_author_meta('description').'</p>
	
	<p class="social_icons_profile"><a href="mailto:'.get_the_author_meta('email').'" title="Send an Email to the Author of this Post"><i class="fa fa-envelope-o fa-2x"></i></a>';
	
	//Fetch the User Social Contact Infomation
	global $post;
	$abap_twitter_url = 'https://twitter.com/'.get_the_author_meta( 'twitter' );
	$abap_google_url = get_the_author_meta( 'googleplus' );
	$abap_linkedin_url = get_the_author_meta( 'linkedin' );	
	$abap_instagram_url = get_the_author_meta( 'instagram' );

	
	if($abap_twitter_url){	
		$abap_twitter_url='&nbsp;<a rel="me nofollow" href="' . esc_url($abap_twitter_url) . '" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a>';
	}else {
		$abap_twitter_url='';
	}
	
	if($abap_google_url){
		$abap_google_url='&nbsp;<a  rel="me nofollow" href="' . esc_url($abap_google_url) . '" target="_blank"><i class="fa fa-google-plus-square fa-2x"></i></a>';
	} else {
		$abap_google_url='';
	}
	if($abap_linkedin_url){
		$abap_linkedin_url='&nbsp;<a  rel="me nofollow" href="' . esc_url($abap_linkedin_url) . '" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></a>';
	} else {
		$abap_linkedin_url='';
	}	
	
 	if($abap_instagram_url){
		$abap_instagram_url='&nbsp;&#8226;&nbsp;<a  rel="me nofollow" href="' . esc_url($abap_instagram_url) . '" target="_blank">Instagram</a>';
	} else {
		$abap_instagram_url='';
	}	
	

	//Output
	if(is_single()) {

		$content.= ($author_box.$abap_twitter_url.$abap_linkedin_url.$abap_google_url.$abap_instagram_url.'</p></div>');
    }
	
    return $content;
}